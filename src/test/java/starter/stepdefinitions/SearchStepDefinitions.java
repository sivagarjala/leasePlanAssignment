package starter.stepdefinitions;

import api.TestApiMethods;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.core.Serenity;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Steps;
import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;

public class SearchStepDefinitions {

    @Steps
    public TestApiMethods testApiMethods;

    @Given("The heroku server application is running")
    public void setApplicationStatus() {
        String env=System.getenv("RUN_ENV");
        SerenityRest.setDefaultBasePath(getBaseURL(env));
        System.out.println("This is URL: " + SerenityRest.getDefaultBasePath());
    }



    @When("^user request for a input with (.*) as (valid||invalid) product$")
    public void getResponseForAProduct(String productName, String productType) {
        testApiMethods.getProductDetails(productName);
    }

    @Then("^user receives a (.*) response$")
    public void validateResponse(String responseType) {
        if(responseType.equalsIgnoreCase("valid"))
            assertThat(Serenity.sessionVariableCalled("GetProductDetailsResponseStatusCode").equals(200)).as("status code is verified");
        else
            assertThat(Serenity.sessionVariableCalled("GetProductDetailsResponseStatusCode").equals(404)).as("status code is verified");
    }

    @Then("^title for each (.*) is displayed as expected$")
    public void verifyResponseData(String productName) {
        assertThat(Serenity.sessionVariableCalled("GetProductDetailsResponseStatusCode").equals(200)).as("status code is verified");
        assertThat(testApiMethods.verifyTitleContent(productName)).isTrue().as("first title has mango");
    }

    @Then("^error message is displayed$")
    public void verifyErrorMessage() {
        assertThat(testApiMethods.isErrorDisplayed()).isTrue().as("Error Message Verification");
    }

    private String getBaseURL(String env) {
        if (env == null || env.isEmpty()) {
            return "https://waarkoop-server.herokuapp.com/api/v1"; //Default URL when env is empty
        } else {
            switch (env) {
                case "localhost":
                    return "https://localhost:8080";
                case "test":
                    return "https://waarkoop-server.herokuapp.com/api/v1";
                case "test-staging":
                    return "PROVIDE STAGING URL";
                case "prod-live":
                    return "PROVIDE LIVE URL";
                case "mock":
                    return "https://f37d3c1b-d7b1-4cc7-b0d2-c1e72f34c017.mock.pstmn.io";
                default:
                    return "https://waarkoop-server.herokuapp.com/api/v1"; //Default URL when env isn't matching
            }
        }
    }
}
